#include "px4_imu/px4_imu_raw.h"
#include "px4_imu/coordinates.h"
#include "px4_imu/kalman.h"
#include <Eigen/Dense>
#include <Eigen/Geometry>

using namespace Eigen;

	void kalman_initialize(px4_imu::coordinates& px4_imu_kalman, px4_imu::kalman& x, px4_imu::kalman& y, float dt);
	void kalman_prediction(px4_imu::px4_imu_raw& px4_imu_data,Vector3f& accel_ef, px4_imu::kalman& x, px4_imu::kalman& y, float dt);
	void kalman_gain(px4_imu::kalman& x, px4_imu::kalman& y);
	void kalman_update(px4_imu::coordinates& px4_imu_kalman, px4_imu::kalman& x, px4_imu::kalman& y,float dt,px4_imu::px4_imu_raw& px4_imu_data, Vector3f& vector_px4_ef);
		

