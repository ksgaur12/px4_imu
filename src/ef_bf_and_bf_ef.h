#include "px4_imu/px4_imu_raw.h"
#include "sensor_msgs/Imu.h"
#include <Eigen/Dense>
#include <Eigen/Geometry>

using namespace Eigen;

void transform_required_data_bf_ef(px4_imu::px4_imu_raw& px4_imu_data, geometry_msgs::Vector3& gravity_bf_from_ef,Vector3f& accel_bf,Vector3f& accel_ef,Vector3f& velocity_px4_bf,Vector3f& velocity_px4_ef);
