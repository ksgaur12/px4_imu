#include "kal.h"
#include "px4_imu/px4_imu_raw.h"
#include "px4_imu/coordinates.h"
#include "px4_imu/kalman.h"
#include <Eigen/Dense>
#include <Eigen/Geometry>

using namespace Eigen;
using namespace std;

void kalman_initialize(px4_imu::coordinates& px4_imu_kalman, px4_imu::kalman& x, px4_imu::kalman& y, float dt){
	
	px4_imu_kalman.int_x = 0;
	px4_imu_kalman.int_y = 0;	
	x.s = 0.0;
	x.v = 0.0;
	 
	x.p1 = 0.0;
	x.p2 = 0.0;
	x.p3 = 0.0;
	x.p4 = 0.0;

	//x.w1 = 0;x.w2 = 0;x.w3 = 0;x.w4 = 0;

	x.w1 = dt*dt*dt*dt/4*0.3954*0.3954*.10;
	x.w2 = dt*dt*dt/2*0.3954*0.3954*.10;
	x.w3 = dt*dt*dt/2*0.3954*0.3954*.10;
	x.w4 = dt*dt*0.3954*0.3954*.10;

	x.r = 0.0172*0.0172;


	y.s = 0.0;
	y.v = 0.0;
	 
	y.p1 = 0.0;
	y.p2 = 0.0;
	y.p3 = 0.0;
	y.p4 = 0.0;

	//y.w1 = 0;y.w2 = 0;y.w3 = 0;y.w4 = 0;

	y.w1 = dt*dt*dt*dt/4*0.2384*0.2384*10;
	y.w2 = dt*dt*dt/2*0.2384*0.2384*10;
	y.w3 = dt*dt*dt/2*0.2384*0.2384*10;
	y.w4 = dt*dt*0.2384*0.2384*10;

	y.r = 0.0957*0.0957;
}


void kalman_prediction(px4_imu::px4_imu_raw& px4_imu_data,Vector3f& accel_ef, px4_imu::kalman& x, px4_imu::kalman& y, float dt ){


	x.acc = accel_ef(0)* 0.1;
	y.acc = accel_ef(1)* 0.1;
	
	//cout << accel_ef <<endl;cout << endl;

	x.s = x.s + x.v * dt + 0.5*dt*dt*x.acc;
	x.v = x.v + dt*x.acc; 

	x.q1 = x.p1 + dt*(x.p2 + x.p3) + dt*dt*x.p4 + x.w1;
	x.q2 = x.p2 + dt*x.p4 + x.w2;
	x.q3 = x.p3 + dt*x.p4 + x.w3;
	x.q4 = x.p4 + x.w4;

	x.S = x.p4 + x.w4 + x.r;


	y.s = y.s + y.v * dt + 0.5*dt*dt*y.acc;
	y.v = y.v + dt*y.acc; 
	
	/*cout << x.s <<"\t";
	cout << x.v <<"\t";
	cout << x.acc <<"\t";
	cout << dt <<"\t";
	cout << px4_imu_data.linear_accel.y<<endl;*/


	y.q1 = y.p1 + dt*(y.p2 + y.p3) + dt*dt*y.p4 + y.w1;
	y.q2 = y.p2 + dt*y.p4 + y.w2;
	y.q3 = y.p3 + dt*y.p4 + y.w3;
	y.q4 = y.p4 + y.w4;

	y.S = y.p4 + y.w4 + y.r;
}


void kalman_gain(px4_imu::kalman& x, px4_imu::kalman& y){

	x.k1 = x.q2/x.S;
	x.k2 = x.q4/x.S;

	y.k1 = y.q2/y.S;
	y.k2 = y.q4/y.S;
}


void kalman_update(px4_imu::coordinates& px4_imu_kalman, px4_imu::kalman& x, px4_imu::kalman& y, float dt,px4_imu::px4_imu_raw& px4_imu_data, Vector3f& velocity_px4_ef){

	//cout <<px4_imu_data.velocity_y<<"\t"<<velocity_px4_ef(0)<<endl;

	x.s = x.s + x.k1*(velocity_px4_ef(0) - x.v);
	x.v = x.v + x.k2*(velocity_px4_ef(0) - x.v);

	y.s = y.s + y.k1*(velocity_px4_ef(1) - y.v);
	y.v = y.v + y.k2*(velocity_px4_ef(1) - y.v);  //x axis and y axis of px4_flow and imu are opposite

	x.p1 = x.q1 - x.k1*x.q3;
	x.p2 = x.q2 - x.k1*x.q4;
	x.p3 = (1-x.k2)*x.q3;
	x.p4 = (1-x.k2)*x.q4;

	y.p1 = y.q1 - y.k1*y.q3;
	y.p2 = y.q2 - y.k1*y.q4;
	y.p3 = (1-y.k2)*y.q3;
	y.p4 = (1-y.k2)*y.q4;


	px4_imu_kalman.vel_x = x.v;
	px4_imu_kalman.vel_y = y.v;
	px4_imu_kalman.int_x = px4_imu_kalman.int_x + dt*x.v;
	px4_imu_kalman.int_y = px4_imu_kalman.int_y + dt*y.v;
	px4_imu_kalman.acc_x = velocity_px4_ef(1);
	px4_imu_kalman.acc_y = velocity_px4_ef(0);
} 

