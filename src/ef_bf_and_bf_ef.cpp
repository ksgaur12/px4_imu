#include "px4_imu/px4_imu_raw.h"

#include <Eigen/Dense>
#include <Eigen/Geometry>
#include "sensor_msgs/Imu.h"

using namespace Eigen;
using namespace std;
typedef Matrix<float,3,3> Matrix3; 


Vector3f gravity_ef, gravity_bf  ;
MatrixXf rotation_matrix_transpose, rotation_matrix;


Quaternionf quat, quat_normalize;
float q0,q1,q2,q3;

void get_gravity_in_bf(geometry_msgs::Vector3& gravity_bf_from_ef){

	gravity_ef << 0, 0, 1;
	gravity_bf = rotation_matrix_transpose * gravity_ef;
	gravity_bf = 100 * gravity_bf;
	
	gravity_bf_from_ef.x = gravity_bf(0);
	gravity_bf_from_ef.y = gravity_bf(1);
	gravity_bf_from_ef.z = gravity_bf(2);

}

void get_angles(){

	cout << "roll      "<<  (atan2(2*(q0*q1+q2*q3),(1-2*(q1*q1+q2*q2))))*180/3.14 << endl;
	cout << "pitch     "<<  (asin(2*(q0*q2-q1*q3)))*180/3.14 << endl;	
	cout << "yaw       "<<  (atan2(2*(q0*q3+q1*q2),(1-2*(q2*q2+q3*q3))))*180/3.14 << endl;	
	cout << endl;

}

void get_accel_ef(px4_imu::px4_imu_raw& px4_imu_data, Vector3f& accel_bf, Vector3f& accel_ef){
	
	px4_imu_data.linear_accel.x = -1*px4_imu_data.linear_accel.x;
	px4_imu_data.linear_accel.y = -1*px4_imu_data.linear_accel.y;

	accel_bf << px4_imu_data.linear_accel.x, px4_imu_data.linear_accel.y, px4_imu_data.linear_accel.z;
	accel_ef = rotation_matrix * accel_bf;
	/*cout << accel_ef << endl;
	cout << endl; */

}

void get_velociyt_px4flow_ef(px4_imu::px4_imu_raw& px4_imu_data, Vector3f& velocity_px4_bf,Vector3f& velocity_px4_ef){
	
	velocity_px4_bf << px4_imu_data.velocity_y, px4_imu_data.velocity_x, 0;
	velocity_px4_ef = rotation_matrix * velocity_px4_bf;

}

void transform_required_data_bf_ef(px4_imu::px4_imu_raw& px4_imu_data,geometry_msgs::Vector3& gravity_bf_from_ef,Vector3f& accel_bf,Vector3f& accel_ef,Vector3f& velocity_px4_bf,Vector3f& velocity_px4_ef){
	

	quat = Quaternion<float,AutoAlign>(px4_imu_data.orien.w, px4_imu_data.orien.x ,px4_imu_data.orien.y ,px4_imu_data.orien.z);	
	quat_normalize = quat.normalized();

	rotation_matrix = quat_normalize.toRotationMatrix();
	rotation_matrix_transpose = rotation_matrix.transpose();
	
	q0 = quat_normalize.w();
	q1 = quat_normalize.x();
	q2 = quat_normalize.y();
	q3 = quat_normalize.z();

	get_gravity_in_bf(gravity_bf_from_ef);
	//get_angles();
	get_accel_ef(px4_imu_data, accel_bf, accel_ef);
	get_velociyt_px4flow_ef(px4_imu_data, velocity_px4_bf, velocity_px4_ef);



}
