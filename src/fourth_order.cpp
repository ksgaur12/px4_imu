#include "fourth_order.h"
#include "sensor_msgs/Imu.h"
#include "std_msgs/String.h"

#define XAXIS 0
#define YAXIS 1
#define ZAXIS 2

struct fourthOrderFilter{

	float  inputTm1,  inputTm2,  inputTm3,  inputTm4;
	float outputTm1, outputTm2, outputTm3, outputTm4; 

}fourthOrder[3];



void initialize_accel(struct fourthOrderFilter *filterParameters){
	
  filterParameters->inputTm4 = 0;
  filterParameters->inputTm3 = 0;
  filterParameters->inputTm2 = 0;
  filterParameters->inputTm1 = 0;
  
  filterParameters->outputTm4 = 0;
  filterParameters->outputTm3 = 0;
  filterParameters->outputTm2 = 0;
  filterParameters->outputTm1 = 0;

}


void forth_order_initialize(){

	initialize_accel(&fourthOrder[XAXIS]);
	initialize_accel(&fourthOrder[YAXIS]);
	initialize_accel(&fourthOrder[ZAXIS]);
}

float computeFourthOrder(float currentInput,struct fourthOrderFilter *filterParameters)
{
  
  #define _b0  0.001893594048567
  #define _b1 -0.002220262954039
  #define _b2  0.003389066536478
  #define _b3 -0.002220262954039
  #define _b4  0.001893594048567
  
  #define _a1 -3.362256889209355
  #define _a2  4.282608240117919
  #define _a3 -2.444765517272841
  #define _a4  0.527149895089809
  
  float output;
  
  output = _b0 * currentInput                + 
           _b1 * filterParameters->inputTm1  + 
           _b2 * filterParameters->inputTm2  +
           _b3 * filterParameters->inputTm3  +
           _b4 * filterParameters->inputTm4  -
           _a1 * filterParameters->outputTm1 -
           _a2 * filterParameters->outputTm2 -
           _a3 * filterParameters->outputTm3 -
           _a4 * filterParameters->outputTm4;

  filterParameters->inputTm4 = filterParameters->inputTm3;
  filterParameters->inputTm3 = filterParameters->inputTm2;
  filterParameters->inputTm2 = filterParameters->inputTm1;
  filterParameters->inputTm1 = currentInput;
  
  filterParameters->outputTm4 = filterParameters->outputTm3;
  filterParameters->outputTm3 = filterParameters->outputTm2;
  filterParameters->outputTm2 = filterParameters->outputTm1;
  filterParameters->outputTm1 = output;
    
  return output;
}


void fourthOrderFilter(geometry_msgs::Vector3 accel, geometry_msgs::Vector3& accel_filtered)
{

  accel_filtered.x=computeFourthOrder(accel.x,&fourthOrder[XAXIS]);
  accel_filtered.y=computeFourthOrder(accel.y,&fourthOrder[YAXIS]);
  accel_filtered.z=computeFourthOrder(accel.z,&fourthOrder[ZAXIS]);

}


